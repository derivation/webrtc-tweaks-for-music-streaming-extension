# WebRTC tweaks for music streaming

This chrome extension introduces a toggle to enhance sound quality when
streaming music over WebRTC.

Currently this means:

 * disabling automatic gain,
 * disabling echo cancellation,
 * disabling noise suppression,
 * enabling stereo and CD quality,
 * forcing high bitrate during stream negotiation.

Enabling this extension is done on a per-domain basis. For instructions on how
to use the extension, please visit the [Usage page](usage.html), always
available by clicking on the Extensions icon in the Chrome toolbar then finding
"WebRTC tweaks for music streaming" → "⋮" More Actions → "Usage".

This extension is based on the [Disable Automatic Gain
Control](https://github.com/joeywatts/gmeet-disable-autogain-control-extension)
by Joey Watts, and also draws from a collection of examples gathered by Gustavo
Garcia on <a href="https://github.com/ggarber/webrtc-intercept/">how to
intercept WebRTC APIs in the browser</a>.
