// Show context menu that allows enabling/disabling on a per-domain basis.
chrome.browserAction.onClicked.addListener(tab => {
    const { origin } = new URL(tab.url);
    chrome.permissions.contains({
        origins: [origin + "/*"],
    }, (hasPermission) => {
        if (hasPermission) {
            chrome.permissions.remove({
                origins: [origin + "/*"]
            }, () => chrome.tabs.reload(tab.id));
        } else {
            chrome.permissions.request({
                origins: [origin + "/*"]
            }, () => chrome.tabs.reload(tab.id));
        }
    });
});

chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    injectScriptIfNecessary(tab);
});

/**
 * @param {chrome.tabs.Tab} tab 
 */
function injectScriptIfNecessary(tab) {
    if (tab.status !== "loading" || !tab.url) {
        return;
    }

    try {
        const { origin, protocol } = new URL(tab.url);
        if (protocol !== "https:" && protocol !== "http:") {
            return;
        }
        chrome.permissions.contains({
            origins: [origin + "/*"]
        }, (hasPermission) => {
            if (hasPermission) {
                chrome.tabs.executeScript(tab.id, {
                    runAt: "document_start",
                    allFrames: true,
                    file: "installWebRTCAudioTweaks.js",
                });
            }
            chrome.browserAction.setTitle({
                title: hasPermission
                    ? "Enable WebRTC tweaks for music streaming"
                    : "Disable WebRTC tweaks for music streaming",
                tabId: tab.id,
            });
            chrome.browserAction.setBadgeText({
                text: hasPermission ? "On" : "",
                tabId: tab.id,
            });
        });
    } catch (e) {
        console.error("Failed to inject script", e);
    }
}

function showUsage() {
    chrome.tabs.create({
        url: chrome.extension.getURL("usage.html")
    });
}

chrome.runtime.onMessage.addListener((message, sender, sendResponse) => {
    if (typeof message === "object" && message["type"] === "enable-meet-hangouts") {
        chrome.permissions.request({
            origins: [
                "https://meet.google.com/*",
                "https://hangouts.google.com/*"
            ]
        }, (granted) => {
            sendResponse(granted);
        });
        return true;
    }
});

chrome.contextMenus.create({
    title: "Usage",
    contexts: ["browser_action"],
    onclick: () => {
        showUsage();
    }
});

chrome.runtime.onInstalled.addListener(({ reason, previousVersion }) => {
    if (reason === "install") {
        showUsage();
    }
});
