(function() {
    function setLegacyChromeConstraint(constraint, name, value) {
        if (constraint.mandatory && name in constraint.mandatory) {
            constraint.mandatory[name] = value;
            return;
        }
        if (constraint.optional) {
            const element = constraint.optional.find(opt => name in opt);
            if (element) {
                element[name] = value;
                return;
            }
        }
        // `mandatory` options throw errors for unknown keys, so avoid that by
        // setting it under optional.
        if (!constraint.optional) {
            constraint.optional = [];
        }
        constraint.optional.push({ [name]: value });
    }
    function setConstraint(constraint, name, value) {
        if (constraint.advanced) {
            const element = constraint.advanced.find(opt => name in opt);
            if (element) {
                element[name] = value;
                return;
            }
        }
        constraint[name] = value;
    }
    function tweakAudioConstraints(constraints) {
        console.log("Tweaking audio constraints for music streaming!", constraints);
        if (constraints && constraints.audio) {
            if (typeof constraints.audio !== "object") {
                constraints.audio = {};
            }
            if (constraints.audio.optional || constraints.audio.mandatory) {
                setLegacyChromeConstraint(constraints.audio, "googAutoGainControl", false);
                setLegacyChromeConstraint(constraints.audio, "googAutoGainControl2", false);
                setLegacyChromeConstraint(constraints.audio, "googEchoCancellation", false);
                setLegacyChromeConstraint(constraints.audio, "googEchoCancellation2", false);
                setLegacyChromeConstraint(constraints.audio, "googHighpassFilter", false);
                setLegacyChromeConstraint(constraints.audio, "googNoiseSuppression", false);
                setLegacyChromeConstraint(constraints.audio, "googNoiseSuppression2", false);
                setLegacyChromeConstraint(constraints.audio, "echoCancellation", false);
                setLegacyChromeConstraint(constraints.audio, "channelCount", 2);
                setLegacyChromeConstraint(constraints.audio, "sampleRate", 48000);
                setLegacyChromeConstraint(constraints.audio, "sampleSize", 16);
            } else {
                setConstraint(constraints.audio, "googAutoGainControl", false);
                setConstraint(constraints.audio, "googAutoGainControl2", false);
                setConstraint(constraints.audio, "googEchoCancellation", false);
                setConstraint(constraints.audio, "googEchoCancellation2", false);
                setConstraint(constraints.audio, "googHighpassFilter", false);
                setConstraint(constraints.audio, "googNoiseSuppression", false);
                setConstraint(constraints.audio, "googNoiseSuppression2", false);
                setConstraint(constraints.audio, "autoGainControl", false);
                setConstraint(constraints.audio, "echoCancellation", false);
                setConstraint(constraints.audio, "noiseSuppression", false);
                setConstraint(constraints.audio, "sampleRate", 48000);
                setConstraint(constraints.audio, "sampleSize", 16);
                setConstraint(constraints.audio, "volume", 1.0);
                setConstraint(constraints.audio, "channelCount", 2);
            }
        }
    }

    function patchFunction(object, name, createNewFunction) {
        if (name in object) {
            var original = object[name];
            object[name] = createNewFunction(original);
        }
    }

    patchFunction(navigator.mediaDevices, "getUserMedia", function (original) {
        return function getUserMedia(constraints) {
            tweakAudioConstraints(constraints);
            return original.call(this, constraints);
        };
    });
    function patchDeprecatedGetUserMedia(original) {
        return function getUserMedia(constraints, success, error) {
            tweakAudioConstraints(constraints);
            return original.call(this, constraints, success, error);
        };
    }
    patchFunction(navigator, "getUserMedia", patchDeprecatedGetUserMedia);
    patchFunction(navigator, "mozGetUserMedia", patchDeprecatedGetUserMedia);
    patchFunction(navigator, "webkitGetUserMedia", patchDeprecatedGetUserMedia);
    patchFunction(MediaStreamTrack.prototype, "applyConstraints", function (original) {
        return function applyConstraints(constraints) {
            tweakAudioConstraints(constraints);
            return original.call(this, constraints);
        };
    });

    // Huge thanks for the inspiration:
    // https://github.com/ggarber/webrtc-intercept
    var origPeerConnection = window.RTCPeerConnection;
    var newPeerConnection = function(config, constraints) {
        console.log('PeerConnection created with config', config);

        var pc = new origPeerConnection(config, constraints);
        var origSetLocalDescription = pc.setLocalDescription.bind(pc);
        pc.setLocalDescription = async function(sdp, success, failure) {
            if (sdp.sdp.match(/useinbandfec=1/)) {
              sdp.sdp = sdp.sdp.replace("useinbandfec=1", "useinbandfec=1; cbr=1; usedtx=1; stereo=1; sprop-stereo=1; maxaveragebitrate=510000");
              console.log('SDP tweaked for music streaming', sdp.sdp);
            }
            if (success === undefined) {
                return await origSetLocalDescription(sdp);
            }
        };
        return pc;
    };

    window['RTCPeerConnection'] = newPeerConnection;
    // Copy the static methods
    Object.keys(origPeerConnection).forEach(function(x) {
        window['RTCPeerConnection'][x] = origPeerConnection[x];
    });
    window['RTCPeerConnection'].prototype = origPeerConnection.prototype;

    console.log(
        "WebRTC tweaks for music streaming loaded! (based on Disable Autogain by Joey Watts)");
})();
