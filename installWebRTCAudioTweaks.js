(function () {
    if (window.WEBRTC_TWEAKS_INSTALLED) {
        return;
    }
    var s = document.createElement('script');
    s.src = chrome.runtime.getURL('webrtcAudioTweaks.js');
    s.onload = function() {
        this.remove();
    };
    (document.head || document.documentElement).prepend(s);
    console.log("Installed <script> tag for WebRTC tweaks for music streaming");
})();
var WEBRTC_TWEAKS_INSTALLED = true;
